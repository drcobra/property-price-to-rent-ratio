import numpy as np
import pandas as pd

from rightmove_webscraper import rightmove_data
from pandas import DataFrame


def getSummary(filename, url):
    print("Connecting URL: {} ...".format(url))
    try:
        data = None
        data = rightmove_data(url)#Rent Brighton & Hove

        #pd.select = data.get_results[["price","type","address","postcode","number_bedrooms"]]
        selectData = data.get_results[["price","postcode","number_bedrooms"]]
        output = selectData.groupby(["postcode","number_bedrooms"]).agg({"price":["mean", "count"]}).astype(int)
        output.columns = ["_".join(x) for x in output.columns.ravel()]

        output.to_csv(filename, sep=",", encoding="utf-8")
        return output

    except Exception as e:
        print("URL: {}\n> FAILED with Exception:\n\t{}".format(url, e))
    print()


# Rent
rentSummary = getSummary("rent-as-bedrooms.csv", "https://www.rightmove.co.uk/property-to-rent/find.html?locationIdentifier=REGION%5E61480&maxBedrooms=6&includeLetAgreed=false&dontShow=houseShare%2Cretirement")
rentSummary.columns = ["rent_price", "rent_num"]
print(rentSummary)
# Buy
buySummary = getSummary("buy-as-bedrooms.csv", "https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E61480&maxBedrooms=6&includeSSTC=false&dontShow=retirement%2CsharedOwnership")
buySummary.columns = ["buy_price", "buy_num"]
print(buySummary)
