import numpy as np
import pandas as pd
#import csv


buyData = pd.read_csv("buy-as-bedrooms.csv")
print(buyData)

rentData = pd.read_csv("rent-as-bedrooms.csv")
print(rentData)

summary = pd.DataFrame()

for index, row in buyData.iterrows():
    buyMean = row["price_mean"]
    rentMean = rentData[ (rentData["postcode"] == row["postcode"]) & (rentData["number_bedrooms"] == row["number_bedrooms"]) ].iloc[0]["price_mean"]

    ratio = rentMean / buyMean * 100

    summary = summary.append({"postcode": row["postcode"], "bedrooms": row["number_bedrooms"], "ratio": ratio}, ignore_index=True)


summary.to_csv("property-ratio.csv", sep=",", encoding="utf-8" ,header=True ,columns=["postcode","bedrooms","ratio"])
output = summary.groupby(["postcode","bedrooms"])

print(output.describe())
